package com.viartemev.paint_shop

import com.viartemev.paint_shop.domain.ColorType
import com.viartemev.paint_shop.domain.ColorType.GLOSSY
import com.viartemev.paint_shop.domain.Customer
import com.viartemev.paint_shop.domain.Solution
import com.viartemev.paint_shop.utils.canContain

class PaintShop {

    fun planBatch(batchNumber: Int, colorsLimit: Int, customers: List<Customer>): Solution {
        val plan = planBatch(customers.sortedBy { it.preferredColors.size }, mapOf(), colorsLimit)
        return if (!plan.second)
            Solution(batchNumber, mapOf())
        else {
            val map = plan.first.toMutableMap()
            (1..colorsLimit).toList().forEach({ map.putIfAbsent(it, GLOSSY) })
            Solution(batchNumber, map)
        }
    }

    private fun planBatch(customers: List<Customer>, colorMap: Map<Int, ColorType>, colorsLimit: Int): Pair<Map<Int, ColorType>, Boolean> {
        if (customers.isEmpty()) return colorMap to true

        for (preferredColor in customers[0].preferredColors.sortedBy { it.colorType }) {
            if (preferredColor.number > colorsLimit
                    || (colorMap.size >= colorsLimit && !colorMap.contains(preferredColor.number))) return colorMap to false

            if (colorMap.canContain(preferredColor)) {
                val remaining = planBatch(customers.drop(1), colorMap + (preferredColor.number to preferredColor.colorType), colorsLimit)
                if (remaining.second) return remaining
                else continue
            } else continue
        }
        return colorMap to false
    }

}