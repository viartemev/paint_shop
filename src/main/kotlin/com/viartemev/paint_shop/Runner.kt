package com.viartemev.paint_shop

import com.viartemev.paint_shop.utils.printSolutions
import java.io.File


fun main(args: Array<String>) {
    if (args.size != 1) {
        throw RuntimeException("Set the file name as argument")
    }
    File(args[0]).printSolutions()
}