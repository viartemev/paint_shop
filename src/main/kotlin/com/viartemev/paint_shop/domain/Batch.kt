package com.viartemev.paint_shop.domain

data class Batch(val countOfColors: Int, val customers: List<Customer>)
