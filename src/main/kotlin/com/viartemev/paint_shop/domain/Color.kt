package com.viartemev.paint_shop.domain

data class Color(val number: Int, val colorType: ColorType)
