package com.viartemev.paint_shop.domain

enum class ColorType(var number: Int) {
    GLOSSY(0),
    MATTE(1);

    companion object {
        fun find(type: Int): ColorType {
            return when (type) {
                0 -> GLOSSY
                1 -> MATTE
                else -> throw RuntimeException("Can't find color type by $type")
            }
        }
    }
}