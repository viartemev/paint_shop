package com.viartemev.paint_shop.domain

data class Customer(val preferredColors: List<Color>)