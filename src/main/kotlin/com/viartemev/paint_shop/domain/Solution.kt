package com.viartemev.paint_shop.domain

data class Solution(val batchNumber: Int, val colors: Map<Int, ColorType>) {

    override fun toString(): String {
        return "Case #$batchNumber: " +
                if (colors.isNotEmpty()) colors.values.map { it.number }.joinToString(" ") else "IMPOSSIBLE"
    }

}