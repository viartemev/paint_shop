package com.viartemev.paint_shop.utils

import com.viartemev.paint_shop.PaintShop
import com.viartemev.paint_shop.domain.Batch
import com.viartemev.paint_shop.domain.Color
import com.viartemev.paint_shop.domain.ColorType
import com.viartemev.paint_shop.domain.Customer
import java.io.File

fun Map<Int, ColorType>.canContain(wish: Color): Boolean {
    val colorType = this[wish.number]
    return colorType == null || colorType == wish.colorType
}

fun File.printSolutions(): Unit {
    val lines = this.readLines()
    val countOfBatches = lines[0].toInt()
    if (countOfBatches <= 0) {
        throw RuntimeException("Count of batches must be more than 0")
    }

    var batchWillStarFromLine = 1
    for (batchNumber in 1..countOfBatches) {
        val parsedBatch = parseBatch(lines.drop(batchWillStarFromLine))
        batchWillStarFromLine += 2 + parsedBatch.customers.size

        val result = PaintShop().planBatch(batchNumber, parsedBatch.countOfColors, parsedBatch.customers)
        println(result)
    }
}

fun parseBatch(batch: List<String>): Batch {
    val countOfColors = batch[0].toInt()
    if (countOfColors <= 0) {
        throw RuntimeException("Count of colors must be more than 0")
    }

    val countOfCustomers = batch[1].toInt()
    if (countOfCustomers <= 0) {
        throw RuntimeException("Count of customers must be more than 0")
    }
    return Batch(countOfColors, batch.subList(2, 2 + countOfCustomers).map { s -> Customer(fetchColors(s)) })
}

fun fetchColors(preferredColorsSource: String): List<Color> {
    val clientPreference = preferredColorsSource.split(' ').map { it.toInt() }

    val countOfColors = clientPreference[0]
    if (countOfColors <= 0) {
        throw RuntimeException("Count of preferred colors must be more than 0")
    }

    if (invalidClientPreferencesList(clientPreference, countOfColors)) {
        throw RuntimeException("Wrong format of client preferences")
    }

    return extractColors(clientPreference.drop(1))
}

fun invalidClientPreferencesList(list: List<Int>, countOfColors: Int) = list.size < 3 || list.size % 2 == 0 || ((list.size - 1) / 2 != countOfColors)

tailrec fun extractColors(preferences: List<Int>, colors: List<Color> = listOf<Color>()): List<Color> {
    if (preferences.isEmpty()) {
        return colors
    }
    return extractColors(preferences.drop(2), colors + listOf(parseColor(preferences.take(2))))
}

fun parseColor(colorInfo: List<Int>): Color {
    return Color(colorInfo[0], ColorType.find(colorInfo[1]))
}