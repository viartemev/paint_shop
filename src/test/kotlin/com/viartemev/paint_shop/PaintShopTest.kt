package com.viartemev.paint_shop

import com.viartemev.paint_shop.domain.Color
import com.viartemev.paint_shop.domain.ColorType.GLOSSY
import com.viartemev.paint_shop.domain.ColorType.MATTE
import com.viartemev.paint_shop.domain.Customer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

open class PaintShopTest {

    @Test
    open fun `possible case, no client's`() {
        val batchNumber = 1
        val colorsLimit = 1

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf())
        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], GLOSSY)
    }

    @Test
    open fun `possible case, one client, one color`() {
        val batchNumber = 1
        val colorsLimit = 1

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(Customer(listOf(Color(1, MATTE)))))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], MATTE)
    }

    @Test
    open fun `impossible case, two clients, one color`() {
        val batchNumber = 1
        val colorsLimit = 1
        val customerWithMatte = Customer(listOf(Color(1, MATTE)))
        val customerWithGlossy = Customer(listOf(Color(1, GLOSSY)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithMatte, customerWithGlossy))

        assertTrue(solution.colors.isEmpty())
    }

    @Test
    open fun `possible case, two clients, one color`() {
        val batchNumber = 1
        val colorsLimit = 1
        val customerWithGlossy = Customer(listOf(Color(1, GLOSSY)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithGlossy, customerWithGlossy))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], GLOSSY)
    }

    @Test
    open fun `possible case, one client, two colors`() {
        val batchNumber = 1
        val colorsLimit = 2
        val customerWithMatte = Customer(listOf(Color(1, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithMatte))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], MATTE)
        assertEquals(solution.colors[2], GLOSSY)
    }

    @Test
    open fun `possible case, two clients, two colors`() {
        val batchNumber = 1
        val colorsLimit = 2
        val customerWithMatte = Customer(listOf(Color(1, GLOSSY)))
        val customerWithGlossy = Customer(listOf(Color(2, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithMatte, customerWithGlossy))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], GLOSSY)
        assertEquals(solution.colors[2], MATTE)
    }

    @Test
    open fun `possible case, one clients with many preferences, six colors`() {
        val batchNumber = 1
        val colorsLimit = 6
        val customerWithManyColors = Customer(listOf(Color(1, GLOSSY), Color(2, MATTE), Color(3, MATTE), Color(4, MATTE), Color(5, MATTE), Color(6, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithManyColors))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], GLOSSY)
    }

    @Test
    open fun `impossible case, one client with one not exists color, one color`() {
        val batchNumber = 1
        val colorsLimit = 1
        val customerWithNotExistsColor = Customer(listOf(Color(6, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithNotExistsColor))

        assertTrue(solution.colors.isEmpty())
    }

    @Test
    open fun `possible case, one client with many colors and one not exists color, one color`() {
        val batchNumber = 1
        val colorsLimit = 1
        val customerWithNotExistsColor = Customer(listOf(Color(1, MATTE), Color(6, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customerWithNotExistsColor))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], MATTE)
    }

    @Test
    open fun `possible case, tree clients, five colors`() {
        val batchNumber = 1
        val colorsLimit = 5
        val customer1 = Customer(listOf(Color(1, MATTE)))
        val customer2 = Customer(listOf(Color(1, GLOSSY), Color(2, GLOSSY)))
        val customer3 = Customer(listOf(Color(5, GLOSSY)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customer1, customer2, customer3))

        assertEquals(solution.colors.size, colorsLimit)
        assertEquals(solution.colors[1], MATTE)
        assertEquals(solution.colors[2], GLOSSY)
        assertEquals(solution.colors[3], GLOSSY)
        assertEquals(solution.colors[4], GLOSSY)
        assertEquals(solution.colors[5], GLOSSY)
    }

    @Test
    open fun `impossible case, tree clients, five colors`() {
        val batchNumber = 1
        val colorsLimit = 5
        val customer1 = Customer(listOf(Color(1, MATTE)))
        val customer2 = Customer(listOf(Color(1, GLOSSY), Color(2, GLOSSY)))
        val customer3 = Customer(listOf(Color(2, MATTE)))

        val solution = PaintShop().planBatch(batchNumber, colorsLimit, listOf(customer1, customer2, customer3))

        assertTrue(solution.colors.isEmpty())
    }

}
