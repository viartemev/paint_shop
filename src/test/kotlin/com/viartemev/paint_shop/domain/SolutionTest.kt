package com.viartemev.paint_shop.domain

import com.viartemev.paint_shop.domain.ColorType.*
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

open class SolutionTest {

    @Test
    open fun `impossible solution`() {
        assertTrue("Case #1: IMPOSSIBLE" == Solution(1, emptyMap()).toString())
    }

    @Test
    open fun `possible solution`() {
        assertTrue("Case #1: 1 1" == Solution(1, mapOf(Pair(1, MATTE), Pair(2, MATTE))).toString())
    }
}