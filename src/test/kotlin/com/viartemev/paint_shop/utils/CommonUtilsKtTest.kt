package com.viartemev.paint_shop.utils

import com.viartemev.paint_shop.domain.Color
import com.viartemev.paint_shop.domain.ColorType
import com.viartemev.paint_shop.domain.ColorType.GLOSSY
import com.viartemev.paint_shop.domain.ColorType.MATTE
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test


open class CommonUtilsKtTest {

    @Test
    fun `canContain with color already exists in map with different color type`() {
        val colorMap = mapOf(Pair(1, MATTE), Pair(2, GLOSSY))
        assertFalse(colorMap.canContain(Color(1, GLOSSY)))
    }

    @Test
    fun `canContain with color already exists in map with same color type`() {
        val colorMap = mapOf(Pair(1, MATTE), Pair(2, GLOSSY))
        assertTrue(colorMap.canContain(Color(2, GLOSSY)))
    }

    @Test
    fun `canContain with empty map`() {
        val colorMap = mapOf<Int, ColorType>()
        assertTrue(colorMap.canContain(Color(2, GLOSSY)))
    }

    @Test
    fun `parseBatch with invalid count of colors`() {
        assertThrows(NumberFormatException::class.java) { parseBatch(listOf("String")) }
    }

    @Test
    fun `parseBatch with invalid count of customers`() {
        assertThrows(NumberFormatException::class.java) { parseBatch(listOf("1", "String")) }
    }

    @Test
    fun `parseBatch with 2 colors`() {
        val fetchedColors = parseBatch(listOf("2", "2", "1 1 0", "1 2 0"))

        assertEquals(2, fetchedColors.countOfColors)
        assertEquals(2, fetchedColors.customers.size)
        assertEquals(1, fetchedColors.customers[0].preferredColors.size)
        assertEquals(1, fetchedColors.customers[0].preferredColors[0].number)
        assertEquals(GLOSSY, fetchedColors.customers[0].preferredColors[0].colorType)
        assertEquals(1, fetchedColors.customers[1].preferredColors.size)
        assertEquals(2, fetchedColors.customers[1].preferredColors[0].number)
        assertEquals(GLOSSY, fetchedColors.customers[1].preferredColors[0].colorType)
    }

    @Test
    fun `fetchColors with invalid string`() {
        val colors = "1 1 1 str"
        assertThrows(NumberFormatException::class.java) { fetchColors(colors) }
    }

    @Test
    fun `fetchColors with invalid count of colors`() {
        val colors = "0 1 1"
        assertThrows(RuntimeException::class.java) { fetchColors(colors) }
    }

    @Test
    fun `fetchColors with wrong format of colors`() {
        val colors = "1 1 1 2 0"
        assertThrows(RuntimeException::class.java) { fetchColors(colors) }
    }

    @Test
    fun `fetchColors with 2 colors`() {
        val colors = "2 1 1 2 0"
        val fetchedColors = fetchColors(colors)

        assertEquals(2, fetchedColors.size)
        assertEquals(1, fetchedColors[0].number)
        assertEquals(MATTE, fetchedColors[0].colorType)
        assertEquals(2, fetchedColors[1].number)
        assertEquals(GLOSSY, fetchedColors[1].colorType)
    }

    @Test
    fun `extractColors with no color`() {
        val colorList = listOf<Int>()
        val colors = extractColors(colorList)

        assertTrue(colors.isEmpty())
    }

    @Test
    fun `extractColors with one color`() {
        val colorList = listOf(1, 1)
        val colors = extractColors(colorList)

        assertEquals(1, colors.size)
        assertEquals(1, colors[0].number)
        assertEquals(MATTE, colors[0].colorType)
    }

    @Test
    fun `extractColors with two colors`() {
        val colorList = listOf(1, 1, 1, 1)
        val colors = extractColors(colorList)

        assertEquals(2, colors.size)
        assertEquals(1, colors[0].number)
        assertEquals(MATTE, colors[0].colorType)
        assertEquals(1, colors[1].number)
        assertEquals(MATTE, colors[1].colorType)
    }

    @Test
    fun `validClientPreferencesList with 2 color`() {
        val colorList = listOf(2, 1, 1, 2, 1)
        assertFalse(invalidClientPreferencesList(colorList, 2))
    }

    @Test
    fun `validClientPreferencesList with invalid 2 color`() {
        val colorList = listOf(1, 1, 1, 1)
        assertTrue(invalidClientPreferencesList(colorList, 1))
    }

}